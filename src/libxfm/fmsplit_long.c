/*
	Abstract:	Implements the split long (.sl) command. The .sl command
				takes the next token and splits it into smaller tokens
				which are placed into the input stream. This allows a long
				URL to be split without the author needing to do so manually.
				i= supplies an insert string, and b= supplies the character(s)
				used to break the token. i= and b= must be given before the
				token so that no end of command marker is needed. The following
				splits the url using +, %, or = characters and adds a .sm command
				between the bits:

					.sl i=.sm b=+=% https://some.site.com/?=some+long+string+that+is+obnoxous

				The result is the following tokens being put back into the input
				stram.

					https://some.site.com/?=some .sm +long .sm +string .sm +that .sm +is .sm +obnoxous

	Date:		5 April 2024
	Author:		E. Scott Daniels
*/

#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

void FMsplitLong( void ) {
	char*	buf;        		// next input token
	int		 len;          		// and its len
	char*	breakStr = NULL;	// characters to break token on
	char*	insStr = NULL;
	char*	tokStr = NULL;
	int		error = 0;

	while( (len = FMgetparm( &buf) ) != 0  ) {
		switch( *buf ) {
			case 'b':		// the break character(s)
				if( (breakStr = strchr( buf, '=' )) == NULL ) {
					FMmsg( E_UNKNOWNPARM, buf );
					error = 1;
				} else {
					breakStr = strdup( breakStr + 1 );	// buf will be overlaid, so must copy
				}
				break;

			case 'i':		// the insert string
				if( (insStr = strchr( buf, '=' )) == NULL ) {
					FMmsg( E_UNKNOWNPARM, buf );
					error = 1;
				} else {
					insStr = strdup( insStr + 1 );
				}
  				break;

			default:		// assume token to split
				tokStr = strdup( buf );
				break;
		}
	}

	TRACE( 2, "longSplit on '%s' insert='%s' errors=%s token=%s\n", 
		breakStr, insStr == NULL ? "none" : insStr, error ? "true" : "false", buf );

	if( error ) {
		return;
	}

	if( len > 0 ) {
		FMcbreak( tokStr, breakStr, insStr ); 
	} else {
		FMmsg( E_PARMOOR, "no token for .sl command" );
	}

	if( tokStr != NULL ) {
		free( tokStr );
	}
	if( breakStr != NULL ) {
		free( breakStr );
	}
	if( insStr != NULL ) {
		free( insStr );
	}
}
