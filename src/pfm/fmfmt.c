/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
* ---------------------------------------------------------------------
* Mnemonic:		fmfmt.c contains routines to add, purge, and otherwise 
*				manipulate the format_blk list.
* Date: 		9 October 2001
* Author:		E. Scott Daniels
*				10 Apr 2007 - Memory leak cleanup 
*				06 Nov 2007 - Pops leftover blocks when restoring state.
*				07 Jul 2013 - cleanup 
*				22 Mar 2016 - Prevent out of range issue on restore.
*				17 Mar 2018 - Correct compiler printf warnings
*				17 Jul 2016 - Bring decls into the modern world.
*				19 Dec 2022 - Add hilite and strikeout support.
* ---------------------------------------------------------------------
*/

#define FMT_STACK_SIZE	25

static struct format_blk *fmt_lst = NULL;	/* pointer to the current format list */
static struct format_blk *fmt_stack[FMT_STACK_SIZE];
static int fmt_idx = 0;

extern char* FMdef_dup( char* ptr, char* def ) {
	if( ptr != NULL ) {
		return strdup( ptr );
	}

	return strdup( def );
}

extern void FMfmt_dump( void )
{
	struct format_blk *f;
	
	for( f = fmt_lst; f; f = f->next )
		fprintf( stderr, "\tfmt: %s %d %s\n", f->font, f->size, f->colour ? f->colour : "no-colour" );
}


/*
	Allow a colour value to be pushed into the head block if there isn't one set. 
	This may be needed if colour values are not preserved over page boundaries.
*/
extern void FMfmt_force_colour( char* colour )
{
	struct format_blk *f;
	
	if( fmt_lst == NULL ) {
		return;
	}

	for( f = fmt_lst; f->next; f = f->next );		// find the head (list is reversed)

	if( f->colour == NULL ) {			// only set if not set
		f->colour = strdup( colour );
	}
}

/*
	push the current list and start a new one
*/
extern int FMfmt_save( void )
{
	TRACE( 2, "fmt_save: idx=%d pushing list=%p\n", fmt_idx, fmt_lst );
	fmt_stack[fmt_idx++] = fmt_lst;
	fmt_lst = NULL;

	if( fmt_idx > FMT_STACK_SIZE )
	{
		fprintf( stderr, "abort: internal error in FMfmt: save/push stack overrun\n" );
		exit( 1 );
	}

	FMfmt_add( );		/* initiate a new list */

	return 0;
}

/*
	Reset the list to what was previously saved.
*/
extern int FMfmt_restore( void )
{
	int size;
	char *font;
	char *colour;
	int start;
	int end;
	int	ydisp;

	while( FMfmt_pop( &size, &font, &colour, &start, &end, &ydisp ) > 0 ) {		/* pop current things */
		if( font ) {
			free( font );
			font = NULL;
		}
		if( colour ) {
			free( colour );
			colour = NULL;
		}
	}

	if( font ) {				// these could come back with defaults if no format block; must free
		free( font );
	}
	if( colour ) {
		free( colour );
	}

	if( fmt_idx > 0 ) {
		fmt_lst = fmt_stack[--fmt_idx];						/* point at old list */
	} else {
		FMfmt_add();
	}
	TRACE( 2, "fmt_restore: idx=%d list=%p\n", fmt_idx, fmt_lst );

	return 0;
}

extern int FMfmt_largest( void )				/* find the largest font in the list */
{
	struct format_blk *f;
	int 	size = textsize;
	
	for( f = fmt_lst; f; f = f->next )
	{
		if( f->size > size )
			size = f->size;
		// this was leaving notible gaps.  TODO - check displacement and adjust size only if it is really needed; general super/sub doesn't need this
	//	if( f->ydisp > 0 )							/* account for super/subscript */
	//		size += f->size - abs( f->ydisp );
	}

	TRACE( 2, "fmt_largest: list=%p size=%d\n", fmt_lst, size );
	return size;
}

/* mark the ending position of the top block */
extern void FMfmt_end( void )
{
	if( fmt_lst )
		fmt_lst->eidx = optr - 2;
}

/*	Add a block to the head of the list. Current font, text size, colour and strike
	settings  are used.
	We add to the head because we need to spit out the the stuff in reverse order
	because ps is stacked 

	The strike/hilight confi (shConfig) is used to pull any currenly set colour
	and area (x,y) settings. Those are captured in the format block.
*/
static void add( int ydisp )
{
	struct format_blk *new = NULL; 
	char*	addType = "new";

	if( fmt_lst && fmt_lst->eidx == optr )		/* no text in output buffer yet; likely an .sf and .st command pair */
	{
		addType = "reset existing";
		new = fmt_lst;			/* just reset the current one */
		new->sidx = optr ? optr-1: 0;
	} else {
		if( (new = (struct format_blk *) malloc( sizeof( struct format_blk ) )) ) {
			memset( new, 0, sizeof( struct format_blk ) );
			new->sidx = optr ? optr-1: 0;
			new->eidx = optr;
			FMfmt_end( );
	
			new->next = fmt_lst;		/* push on */
			fmt_lst = new;
		} else {
			fprintf( stderr, "*** no memory trying to get format block\n" );
			return;
		}
	}

	// ---- common set up regardless of "source" set above -------------------
	if( new->font ) {
		free( new->font );
	}
	new->font = FMdef_dup( curfont, "Helvetica" );
	new->size = textsize;
	new->ydisp = ydisp;

	if( shConfig == NULL || shConfig->flags == FOFL_NONE ) {	// no strike/hiliting, just setup for foreground colour if needed
		new->flags = FOFL_NONE;
		if( textcolour ) {
			if( new->colour ) {
				free( new->colour );
			}
			new->colour = strdup( textcolour );
		}
/*
  else {
			new->colour = NULL;		// it will default to the same colour as is set
		}
*/
	} else {	// must set the colour string with foreground and strike/hilite information
		char*	shColour;
		char	wBuf[1024];
		char*	lastColour;			// the most recent colour b/c we may swap colours to strike/hilite
		char*	strike = "false";
		char*	lc;

		new->flags |= shConfig->flags;			// pull in the strike/hilight flag
		if( shConfig->flags & FOFL_STRIKE ) {
			strike = "true";
		}
		lc = FMget_last_colour();
		lastColour = FMparsecolour(  lc );

		shColour = FMdef_dup( shConfig->colour, "0 0 0" );
		if( shConfig->fixedLen < 0 ) {
			snprintf( wBuf, sizeof( wBuf ), "%s %s %d %s", lastColour,  shColour, shConfig->lWeight, strike );
		} else {
			snprintf( wBuf, sizeof( wBuf ), "%s %s %d %s %d %d", 
				lastColour,  shColour, shConfig->lWeight, strike, shConfig->fixedLen, shConfig->fixedStart );
		}

		if( new->colour != NULL ) {
			free( new->colour );
		}
		new->colour = strdup( wBuf );

		free( lastColour );
		free( lc );
		free( shColour );
	}

	TRACE( 2, "fmt_add: %s: %p eidx=%d optr=%d font=%s size=%d ydisp=%d colour='%s'\n", 
			addType, new, fmt_lst ? fmt_lst->eidx : -99, optr, curfont, textsize, ydisp, safeStr( new->colour ) );
}

/*
	add with no y displacement
*/
extern void FMfmt_add( void )
{
	add( 0 );
}

/*
	add a block with a y displacement value 
*/
extern void FMfmt_yadd( int v )
{
	add( v );
}

/* 
	return the information from the top block and pop the block from the stack.
	NOTE: caller must free font and colour;
*/
extern int FMfmt_pop( int *size, char **font, char **colour, int *start, int *end, int *ydisp )
{
	struct format_blk *next;

	*colour = *font = NULL;

	if( fmt_lst )
	{
		TRACE( 2, "fmt_pop: fmt_lst=%p cury=%d\n", fmt_lst, cury );
		*ydisp = fmt_lst->ydisp;
		*size = fmt_lst->size;
		*font = fmt_lst->font;			/* pass back strings we duped earlier */
		fmt_lst->font = NULL;			// we cant use it anymore
		*colour =  fmt_lst->colour;
		fmt_lst->colour = NULL;		// we can't use it any more
		*start = fmt_lst->sidx;
		*end = fmt_lst->eidx;

		next = fmt_lst->next;			/* remove from queue and purge */

		fmt_lst->font = NULL;
		fmt_lst->colour = NULL;

		free( fmt_lst );
		fmt_lst = next;

		return 1;
	}
	else
	{
		TRACE( 2, "fmt_pop: end ptr=%p cury=%d\n", fmt_lst, cury );
		*ydisp = 0;
		*size = textsize;
		if( *font )
			free( *font );
		*font = strdup( curfont );
		*start = *end = -1;
		*colour = strdup( "000000" );
	}

	return 0;
}

/*
	no text change
	Returns true if any format block contains an underlying font,
	hilight, strikeout or colour change that must be processed even
	when there is no text associated with the instance.
*/
extern int FMfmt_ntChange( ) {
	struct format_blk* f;

	for( f = fmt_lst; f; f = f->next ) {
		if( f->flags & (FOFL_STRIKE | FOFL_HILITE) ) {
			return 1;
		}
	}

	return 0;
}
