.if false
	Mnemonic:	cmd_rst.im
	Abstract:	This is the Common Macro Definition set which when included will
				define macros needed to generate RST output.

				This requires {X}fm version 2.* or greater.

				User may set these variables prior to imbedding this file:
					doc_title		Doc titles placed into output with correct 
					doc_subtitle	annotation. Both are optional, but subttle
									is writen only if doc_title is defined

					simple_dlists	(a simple bold string without any matching
									indention is used for def lists if this is 1.

				Use tfm which will generate RST output. The output might need
				to have the leading blank character stripped before RST will
				properly render it.


	Author:		E. Scott Daniels
	Date:		6 June 2020
.fi

.if ! _cmd_rst_im
.dv _cmd_rst_im 1

.**  TFM defines a character to be 7p wide. These offsets make fixed indention
.**  required by rst easier.
.dv _ch2 14p
.dv _ch3 21p
.dv _ch4 28p
.dv _ch5 35p
.dv _ch6 42p
.dv _ch7 49p
.dv _ch8 56p
.dv _ch9 63p
.dv _ch10 70p

.gv semver
.if &_major 2 <
	.dv __alert ### ABORT ###  rst.im: {X}fm must be 2.0.0 or greater"
	.sv __alert
	.qu
.fi

.** no indention for rst, and no concept of a multi column doc
.dv indent_amt 0
.dv one_col
.dv two_col
.dv cleanup .ca vars _fref.ca ^: .cn showend .dv _no_banner 1 ^:

.** no such thing as centered figure text
.dv fig .fg $1
.dv fig_cen .fg $1

.dv fig_table .fg t=table $1
.dv fig_table_cen .fg t=table $1

.** rst flags no format mode by indention
.dv nf  .sp 1 ^:^: .br .ll -&_ch2 .in +&_ch2
.dv fo  .in -&_ch2 .ll +&_ch2 .sp 1

.** indent with three additional blank characters; whether RST actually indents is unknown
.** no concept of small indent, so just regular
.**
.dv indent .ll -&_ch3 .in +&_ch3
.dv uindent .in -&_ch3 .ll +&_ch3
.dv smindent .ll -&_ch3 .in +&_ch3
.dv smuindent .in -&_ch3 .ll &_ch3

.** list item characters
.dv lic1 *
.dv lic2 -
.dv lic3 +


.** ------ line styling and spacing macros --------------------
.dv esc \$1  		 		: .** if a word ends with _ or * it must be escaped
.dv line_len 
.dv space .sp 1
.dv half_space .sp 1 		: .** RST has no concept of adjusting the between line spacing
.dv break .br |
.dv mult_space .sp $1

.** ------- bullet lists -------------------------
.dv beg_list .sp 1 .bl ${1!*}
.dv end_list .el .sp 1  
.dv li .br .li
.dv li_space ^&half_space .li

.** ------ non-formatted examples ------------------------
.** CAUTION: rst demands a blank line after the example and before the .fg command; in an indention
.**			oriented world this makes no sense as we change the indention level :(
.** 
.dv beg_ex .sp 1 ^:^: .sp 1 .ll -&_ch2 .in +&_ch2 .nf
.dv end_ex .fo on .in -&_ch2 .ll +&_ch2 .sp 1
.dv end_ex_fig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1
.dv end_ex_cfig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1

.dv beg_ex_cen .sp 1 ^:^: .sp 1 .ll -&_ch2 .in +&_ch2 .nf
.dv end_ex_cen .fo on .in -&_ch2 .ll +&_ch2 .sp 1
.dv end_ex_cen_fig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1
.dv end_ex_cen_cfig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1

.** back compat (deprecated)
.dv ex_start .sp 1 ^:^: .sp 1 .ll -&_ch2 .in +&_ch2 .nf
.dv ex_end .fo on .in -&_ch2 .ll +&_ch2 .sp 1
.dv ex_end_fig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1
.dv ex_end_cfig .fo on .in -&_ch2 .ll +&_ch2 .sp 1 .fg $1 ^: .sp 1

.** --------- block centering ----------------------------
.dv center .br $1 .br

.dv beg_center ^.. class:: center .sp 1
.dv end_center .sp 1

.** back compat deprecated
.dv center_start ^.. class:: center .sp 1
.dv center_end .sp 1

.** --------- fonts and font macros ----------------------
.dv colour .dv __unsupported $1 ^:
.dv ital  *${*1}*
.dv bold  **${*1}**
.dv cw    ^^^`^^^`${*1}^^^`^^^`

.** global font changes seem impossible in RST, so set font proportional, ital, bold until
.** reset is not permitted. macros have no effect here.
.dv set_font_cw
.dv set_font_ital
.dv set_font_bold
.dv set_font_normal

.dv just .ju ${1!on}
.dv head_num .hn ${1!on}
.dv pg_num

.** note adds a super script for a column note and is automatically numbered
.** super allows user to add their own superscripted text
.** rst has no concept of a page, so all notes go to the close of the doc
.** 
.dv _note_num 1

.** indirect refs for the _sp macros needed to prevent them from saving with author's macros if '.ca vars' is used
.** .dv super .sm ^:sup^:^`${*1}^`
.dv super ^&_super_sp
.dv super_sp ^&_syper_sp
.dv _super_sp ^:sup^:^`${*1}^`

.** .dv note .sm ^:sup^:^`&{_note_num}^`  .dv _note_num ^[ %.0f ^&_note_num 1 + ] ^:
.dv note ^&_note_sp
.dv note_sp  ^&_note_sp
.dv _note_sp  ^:sup^:^`&{_note_num}^`  .dv _note_num ^[ %.0f ^&_note_num 1 + ] ^:

.dv beg__note	.cn start &atbot Times-Roman 8p ${1!.5i}
.dv end_note	.cn end

.** deprecated
.dv start_note	.cn start &atbot Times-Roman 8p ${1!.5i}

.dv show_notes  .cn show

.** no concept of page, so always imbed now
.dv ifroom .im $2

.** ---- images (not supported at the moment) ------
.dv image ^&space [IMAGE OMITTED] ^&space

.** ----------- definition lists and tables ------------------------------------
.if false
	Using an RST list table to build a definition list seems to render a better
	looking result, but it's still not as nice as PDF or even HTML generated
	by {X}fm.

	def list parms:
		$1 pfm - width of term (e.g. 1i)
		$2 pfm - font, auto etc (e.g. f=&{bold_font!Helvetica-Bold} a=1)
		$3 rst - term/def width (e.g. 15,80)

	{X}fm's auto numbering cannot be used; we can simulate simple aribic numbering
	but it's an inflexible hack.
.fi


.** --------------- definition lists --------------------------------------------
.** Some RST parsers might not support list-tables. We allow ugly work round, but
.** it requires that the author code '.dv simple_dlists 1' to enable it. It
.** will be messy.
.**
.** the beg_dlist macro takes different options depending on the formatter
.** and/or target output.  Postscript uses $1, markdown $2. For RST we default
.** to auto for the cell sizes and don't bother with trying to give the author
.** control (if they want contaol they should just generate HTML and be done with it).
.** 
.** Some RST parsers don't wrap lines in a table (wtf?).  They do seem to break if
.** there is a blank line, so we'll use &_di_term_mode to set double spacing in the
.** term if it is set to .ds.  The macro &force_di_break can be used to set this
.** behaviour.  This is currenlty UNTESTED.

.** default to "off" (single space) unless user invokes force_di_breaks
.dv _di_term_mode_on .ss
.dv _di_term_mode_off .ss
.dv force_di_breaks .dv _di_term_mode_on .ds

.if &{simple_dlists!0} 1 = 
	.dv beg_dlist .bd ${1!1i} ^:
	.dv end_dlist .sp 1
	.dv di        **${*1}**
	.dv di_space  **${*1}**
	.dv adi **^&{_di_term}**  ^: 	.dv di_term ^[ %.0f ^&di_term 1 + ] ^:
.ei
	.** auto numbering is only supported at the top level if lists are nested
	.** be VERY careful when changing these defs; RST is sooooo bloody space senstive.
	.dv beg_dlist ~
		.dv _di_term 1 ^:~
		.br  ~
		^^.. list-table^:^: ~
		.in +&_ch4 ~
		.br ~
		^:widths^: auto ~
		.br ~
		^:class^: borderless ~
		.in +&{_ch4}

	.dv di ^&row **${*1}** ^&col 
	.dv di_space ^&row **${*1}** ^&col 
	.dv adi  ^&row **&{_di_term}** ^&col .dv _di_term ^[ %.0f ^&_di_term 1 + ] ^:

	.dv end_dlist ^&_di_term_mode_off .sp 1 .in -&{_ch8} .sp 1
.fi


.** ----------- tables --------------------------------------------

.** tables takes different options depending on the format generated.
.** postscript uses $1, markdown $2, and rst $3
.** for rst it should be the widths.
.** CAUTION: RST seems to require a blank line before the table start.

.** with borders
.dv beg_table ~
	.sp 1  ~
	^^.. list-table^:^: ~
	.in +&_ch4 ~
	.br ~
	^:widths^: ${3!auto} ~
	.sp 1 ~
	* -  .in +&{_ch4}

.** no-border table
.dv beg_table_nb ~
	.sp 1  ~
	^^.. list-table^:^: ~
	.in +&_ch4 ~
	.br ~
	^:widths^: ${3!auto} ~
	.br ~
	^:class^: borderless ~
	.sp 1 ~
	* -  .in +&{_ch4}

.dv row .sp 1 .in -&{_ch4}  * -  .in +&{_ch4}
.dv col .in -&{_ch2}    -  .in +&{_ch2}
.dv end_table .sp 1 .in -&{_ch8}
.dv table_head

.** ---- add doc titles if supplied, and set header defs _afterwards_ ------
.** annotation syntax is <char><position>
.** we use annotated headers to generate title, then setup the persistant
.** header defs for the remainder of the document.
.**
.if doc_title
	.dh 1 a==both s=0,0
	.h1 &doc_title
	.if doc_subtitle
		.dh 2 a=-both s=0,0
		.h2 &doc_subtitle
	.fi
.fi

.** after titles are out, safe to set the final header defs
.dh 1 a==after s=2,1 i=0 m=0
.dh 2 a=-after s=2,1 i=0 m=0
.dh 3 a=~after s=2,0 i=0 m=0

.dv h1 .h1 ${*1} ^:
.dv h2 .h2 ${*1} ^:
.dv h3 .h2 ${*1} ^:
.dv h4 **${*1*}** ^:

.** ---- finally, misc setup -------------------------------------------------
.ju off
.hn off
.pn off
.in 0
.cd 1 16i i=0i
.ll 14i

.in 0i			.** tfm always leaves a one space indention which might need to be stripped

.if use_txt_symbols
	.im symbol_txt.im
.ei
	.im symbol_rst.im
.fi


.fi
