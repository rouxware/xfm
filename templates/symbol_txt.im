
.if false
	Mnemonic:	symbol_txt.im
	Abstract:	These are plain text definitions for various bits of the symbol
				font. In something like postscript output the result is a nice
				Greek letter or something. In ASCII text these probably don't
				translate well, but prevent undefined variables if source is used
				to generate an ASCII version.

				Greek letters have a lead underbar to prevent issues in the HTML
				world. That's not needed for other output, but to be consistent
				across all cmd_* imbed it must be here too.

	Date:		19 September 2021
	Author: 	E. Scott Daniels
.fi

.** Greek alphabets upper and lower
.dv _ALPHA  <<Alpha>>
.dv _BETA  <<Beta>> 
.dv _CHI  <<Chi>> 
.dv _DELTA  <<Delta> 
.dv _EPSILON  <<Epsilon>> 
.dv _PHI  <<Phi>> 
.dv _GAMMA  <<Gamma>> 
.dv _ETA   <<Eta>> 
.dv _IOTA   <<Iota>> 
.dv _KAPPA   <<Kappa>> 
.dv _LAMBDA   <<Lambda>> 
.dv _MU  <<Mu>> 
.dv _NU  <<Nu>> 
.dv _XI  <<Xi>> 
.dv _OMICRON  <<Omicron>> 
.dv _PI  <<Pi>> 
.dv _THETA   <<Theta>> 
.dv _RHO  <<Rho>> 
.dv _SIGMA  <<Sigma>>
.dv _TAO  <<Tao>> 
.dv _UPSILON  <<Upsilon>> 
.dv _OMEGA  <<Omega>> 
.dv _PSI  <<Psi>> 
.dv _ZETA   <<Zeta>> 

.dv _alpha  <<alpha>> 
.dv _beta  <<beta>> 
.dv _chi  <<chi>> 
.dv _delta  <<delta>> 
.dv _epsilon  <<epsilon>> 
.dv _phi  <<phi>> 
.dv _gamma  <<gamma>> 
.dv _eta   <<eta>> 
.dv _iota   <<iota>> 
.dv _kappa   <<kappa>> 
.dv _lambda   <<lambda>> 
.dv _mu  <<mu>> 
.dv _nu  <<nu>> 
.dv _xi  <<xi>> 
.dv _omicron  <<omicron>> 
.dv _pi  <<pi>> 
.dv _theta   <<theta>> 
.dv _rho  <<rho>> 
.dv _sigma  <<sigma>> 
.dv _tao  <<tao>> 
.dv _upsilon  <<upsilon>> 
.dv _omega  <<omega>> 
.dv _psi  <<psi>> 
.dv _zeta   <<zeta>> 

.** symbols; \xxx pass all the way into the postscript and thus must be octal
.dv DEGREE <<degree>>
.dv PLUS_MINUS  <<plus-minus>>
.dv GEQUAL ^<<greater-equal>>
.dv DOT_PROD <<dot-product>>
.dv NOT_EQUAL <<not-equal>>
.dv THREE_EQUAL  <<equiv>>
.dv EQUIV  <<equiv-to>>
.dv APPROX_EQUAL <<approx-equal>>
.dv ELIPIS ^^...

.dv NIL <<nil>>
.dv INTSECT <<intersects>>
.dv UNION  <<union>>
.dv SUBSET <<subset>>
.dv SUBSET_OR_EQ <subset-or-eq>>
.dv ELEMENT <<element>>
.dv NOT_ELEMENT <<not-element>>
.dv ANGLE <<angle>>

.dv REGITERED  <<reg-trademark>>
.dv COPYRIGHT  <<c>>
.dv TRADE_MARK  <<trade-mark>>
.dv SQ_ROOT  <<radical>>
.dv NOT <<not>>
.dv AND <<and>>
.dv WEDGE <<and>>
.dv OR <<or>>
.dv DEC_WEDGE <<or>>
.dv XOR <<xor>>


.dv EURO  <<euro>>
.dv LEQUAL <<lequal>>
.dv INFINITY <<infinity>>
.dv LE_ARROW <<le-arrow>>
.dv UP_ARROW <<up-arrow>>
.dv RT_ARROW <<rt-arrow>>
.dv DN_ARROW <<dn-arrow>>
