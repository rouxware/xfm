
&h1( Citation and Reference Macros)
To make the creation of a set of references which can easily be shared between multiple
documents, and eliminate the need for any manual management of reference numbers, a set of
reference macros are defined in the &cw(ref_setup.im) imbed file.
The following paragraphs describe how to use these macros.

&space
&h2(Citation Format) 
This macro set assumes that citations in the text are numbers enclosed in square brackets
(e.g. [2]) and that the references appear as a group in a list which is numbered without
the brackets. 
While some conferences and journals may prefer the more cumbersome style of using the author's
name and two digit year (e.g. [Eldridge76]), it is our feeling that this technique was born
out of not wanting to renumber citations in the text if a reference was added. 
We also feel that these long references make it more difficult to read the paper.
Authors are certainly free to write their own macros which accomodate this style.

&h2(The Reference File)
To use this macro set, the author must create an imbed file which contains all of the 
reference information.
This is needed as the file is imbedded twice to avoid requiring the author arrange for two
pass formatting if that is not needed.
The reference file consists of:
&half_space
&indent
&beg_list( &lic1 )
&li An opening, reference setup macro call
&half_space
&li The reference entries
&half_space
&li A reference termination macro call
&end_list
&uindent
&space

&h2(A Reference Definition)
There are various styles desired and/or required for each reference entry, and as such this macro set
makes no effort to enforce any one of them.
As such a reference definition is started with the &cw(ref_def()) macro, and terminates with an &cw(end_ref)
macro.
The parameter to the &cw(ref_def()) macro is the reference name which is usually the first author of the work
being cited, but can be anything that the author desires.

&space
Between these two macro calls, the information that is needed, including any special formatting desired,
is added. 
Figure &ref1_fig contains an illustration of a reference.
&space

.ca start ref1.ca
&half_space  
&beg_ex
   &ref_def( eldridge )
      Eldridge, K. E., Jain,  S. K., 1976.
      &ital(Ring Theory: Proceedings of the Ohio University Conference)
      Marcel Dekker Inc. USA.
   &end_ref
&end_ex
&fig_cen( v=ref1_fig Example reference definition. )
&space
.ca end
&ifroom( 3 : ref1.ca )

The definition in figure &ref1_fig defines a book reference which is given the name &ital(eldridge.)
For this particular set of entries, there is formatting (the book title is rendered in italics), but
the pieces of information are not rendered on separate lines (no line breaks).

&space
The order that the references are placed in the imbed file is the order which they are rendered when
&ital( expanding ) them into the document.
The reference numbers are assigned automatically, and  inserting a reference requires only the reprocessing
of the document through an  {X}fm formatter to reset all of the references in the text.

&h2(Citing A Work)
To cite a referenced work in the text, the name of the reference given on the &cw(ref_def()) macro is used
with a leading ampersand.
This is illustrated in figure &ref2_fig.

&beg_ex_cen
   ...is a popular belief about ring theory [&eldridge].
&end_ex_cen_cfig( v=ref2_fig Citing a work in the text. )
&space

As can be seen in figure &ref2_fig, the author supplies the square brackets, and is free to place the
reference directly against the preceding text, or to separate it with a space; what ever the desired
format of the author is. 

&h2(The Reference Housekeeping Macros)
Two macros are used to setup and close the reference processing when the author's imbed file is
read.
The &cw(beg_references) macro (no parameters) must be placed before any use of the &cw(ref_def()) macro, 
and the &cw(end_references) macro (no parameters) must appear after the last &cw(end_ref()) macro.

&h2(Imbedding The References)
To ensure compatability with future releases, the &cw(init_refs()) and &cw(expand_refs()) macros
should be used.
The &cw(init_refs()) macro takes the imbed file name as the only parameter and must be used before
any references are made in the document.
At the point in the document, probably the last section, where references are to be expanded as a
numbered list, the &cw(expand_refs()) macro is used.
This macro is also given the &bold( same ) reference imbed file as was given to the init macro.


&h2(Sharing References Between Documents)
A set of related documents or papers might naturally share a set of common references, and  a small
adjustment to the single reference imbed file technique is all which is needed to allow the references
to be shared.
To easily share references, each reference is maintained in a separate file, in a common directory.
As an example, the entry in figure &ref1_fig might be stored in the file &cw(eldridge.bib) in
the directory &cw($HOME/papers/refs.) 
A reference imbed file is created for each document, and between the &cw(beg_references) and &cw(end_references)
macros the &cw(.bib) files are imbedded. 
This technique is illustrated in figure &ref3_fig.
&space

.ca start ref3.ca
&ex_start
	.gv e HOME home_dir
	.dv rdir ${home_dir}/papers/refs
	&beg_references
	  .im &{rdir}/arnold.bib	
	  .im &{rdir}/eldridge.bib	
	  .im &{rdir}/fernando.bib	
	  .im &{rdir}/mead.bib	
	  .im &{rdir}/norris.bib	
	&end_references
&end_ex_cfig( v=ref3_fig Using .bib files to share references. )
&space
.ca end
&ifroom( 2 : ref3.ca )

