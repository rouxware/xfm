.** the get variable table 

.pu
&set1col

.gv table
.dv gv_tab &_table

.gv page
.dv gv_tab_page &_page

.st [ &textsize 2 - ]
.sf &text_font
.sp

.ju off
.if &two_col_mode 1 =
	.dv lcw 4.0i
.ei
	.dv lcw 3.0i
.fi

.ta  center b=0 p=5p .75i .5i .8i  &lcw
.th .bc start ^&bold( Value ) .br ^&bold( Name ) .bc end .cl ^: .bc start ^&bold(First) .br ^&bold( Version) .bc end .cl ^: .bc start ^&bold(Variable(s)) .br ^&bold( Created) .bc end .cl ^: .bc start .sp 1 ^&bold(Description) .bc end 
.tr l=2 b=1


.bc start Date .bc end
.cl : .br
.cl : .bc start _date .bc end
.cl : Sets the variable with the date using the format: &cw(12 Jan 2002)

.tr
.bc start date .bc end
.cl : .br
.cl : .bc start _date .bc end
.cl : Sets the variable with the date using the format: &cw(mm/dd/yy)

.tr
.bc start env .bc end
.cl : .br
.cl : .bc start user defined .bc end
.cl : Looks up a system environment variable and sets the user 
defined &x variable to its value. 
Two parameters are required: the first is the environment variable name, 
and the second is the &x variable name to assign the value to.
This is available only on systems that support the concept of environment 
variables. 

.tr
.bc start fig .bc end
.cl : .br
.cl : .bc start _fig .bc end
.cl : Places the figure number that will be given to the &stress(next) figure
when the &c(fg) command is used into the variable. 
(Obsolete; use the v=name option on the &c(fg) command to define a variable (name) and automatically
assign it the figure number.)

.tr
.bc start Fig .bc end
.cl : .br
.cl : .bc start _fig .bc end
.cl : Places the figure number that will be given to the &stress(next) figure
when the &c(fg) command is used into the variable &stress( AND ) will advance
the figure number. 
(Obsolete; use the v=name option on the &c(fg) command to define a variable (name) and automatically
assign it the figure number.)

.tr
.bc start fname .bc end
.cl : .br
.cl : .bc start _fname .bc end
.cl : Sets the name of the current input file into the variable

.tr
.bc start font .bc end
.cl : .br
.cl : .bc start _font .bc end
.cl : Places the current font name into the variable. 

.tr
.bc start host .bc end
.cl : .br
.cl : .bc start _host .bc end
.cl : Places the name of the host into the variable. This is available 
only on systems that support the concept of a host name.

.tr
.bc start page .bc end
.cl : .br
.cl : .bc start _page .bc end
.cl : The variable is set to contain the current page number

.tr
.bc start remain .bc end
.cl : .br
.cl : .bc start 
		.sp 1
		_attop .br
		_iremain  .br
		_lremain .br
		_premain
	 .bc end
.cl : 
	Set various variables related to remaining space on the page .br
	True (1) if the next line will appear at the top of the page .br
	Number of inches remaining on the page .br
	Number of lines, at the current font size, remaining on the page .br
	Number of points remaining on the page

.tr
.bc start lmar .bc end
.cl : .br
.cl : .bc start _lmar .bc end
.cl : The current left margin value in points, is placed into the variable.

.tr
.bc start mdy .bc end
.cl : .br
.cl : .bc start 
		_mon 
		_day
		_year
	.bc end
.cl : Sets these variables with the current date.

.tr
.bc start rmar .bc end
.cl : .br
.cl : .bc start _rmar .bc end
.cl : The current right margin value in points, is placed into the variable.

.tr
.bc start sect .bc end
.cl : .sp 1
	4.9.0, 4.11.0
.cl : 
	.bc start
	.sp
	_sect .br
	_next_h1 .br
	_secttxt .br
	_sectn .br
	.bc end
.cl : 
Generate section related variables. .br
The most recent full section "tag" (e.g. 1.3.8) .br
The next H1 value provided no header number commands alter .br
The last full section header text .br
The last full section header number

.tr  
.bc start semver .bc end
.cl : 4.2.0
.cl :
	.sp 1
	.bc start
	_major .br
	_minor .br
	_patch 
	.bc end
.cl :
	Generate variables with the {X}fm semantic version: .br
	The major number of the build version .br
	The minor number of the build version .br
	The patch levelnumber of the build version .br

.tr
.bc start tsize .bc end
.cl : .br
.cl : .bc start _tsize .bc end
.cl : The variable is set to the current text size (in points).

.tr
.bc start table .bc end
.cl : .br
.cl : .bc start _table .bc end
.cl : The current table number is placed into the variable.

.tr
.bc start Time .bc end
.cl : .br
.cl : .bc start _time .bc end
.cl : The current time, in the format HH:MM:SS, is placed into the variable.

.tr r=5i
.bc start version .bc end
.cl : 4.2.4
.cl : .bc start _ver .bc end 
.cl : 
	A string with the {X}fm version and build date. Semantic version variables
	(see semver) are also generated starting in v4.2.4).

.tr r=5i
.bc start words .bc end
.cl : .br
.cl : .bc start _words .bc end
.cl : The number of words placed into the document is placed into the variable.

.tr r=5i
.bc start y .bc end
.cl : .bc start 
.cl : .br
	_cury  .br
	_topy .br
	_coln
	.bc end 
.cl : 
	Get &cw(y) information: .br
	The current &cw(y) position (points from the top of the page) 
	is placed into the &cw(_cury) variable. 
	The top most &cw(y) value is placed into &cw(_topy) variable and the 
	current column number (0 is the first column) is placed into &cw(_coln.)

.et

.st &textsize
.fg x=center t=table  Get value command parameters and resulting variable definitions.
.sp 2

.tt 
&set2col
.po
