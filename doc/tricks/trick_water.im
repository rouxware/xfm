
&h1( Adding A Watermark )
Some times it is just nice to have a small watermark or logo at the top of each page.
The watermark command has been used on this document to add a small watermark to the
top left of each page after the table of contents.

&h2( The Logo )
The logo can be in one of two forms: an encapsulated postscript file with an image, or
a postscript command set which contains the function(s) needed to draw the logo.
The encapsulated postscript method allows the author to use a drawing tool to create
the logo and save it as a .eps file. This has the advantage of being easy, but can
lead to some size bloat depending on the resulting .eps generation.

.sp
Creating a postscript programme to render a logo is not difficult, but it can be
time consuming, and does require that the author know the language.
The advantage of using a program rather than an image file is that they are generally
much more compact.
The following is the watermark command used to generate the watermark in this
document; appendix &wm_appendix has the postscript code for the watermark itself.

.sp .
&beg_ex
   .wm on rwLogo.mps f=paintLogo x=.25i y=.75i s=.6
&end_ex
.fg x=center Watermark command to generate a watermark by calling the function paintLogo.

